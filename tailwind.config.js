/** @type {import('tailwindcss').Config} */
export default {
    content: ['./src/**/*.{html,js,svelte,ts}'],
    theme: {
        extend: {
            colors: {
                'stone-100': '#F0EFEA',
                'gray-500': '#5E6574',
                'indigo-300': '#AEB1DA',
                'slate-100': '#F5F6FB',
                'neutral-300': '#BDC0CF',
                'green-500': '#00CE6A',
                'violet-200': '#D7D4F1',
                'violet-300': '#b7b8e8',
            },
            spacing: {
                '75': '18.75rem',
                '6.5': '1.625rem'
            },
            fontSize: {
                '4xl': '2.5rem'
            },
            boxShadow: {
                'md': '0 2px 2px 1px rgba(0, 0, 0, 0.1)'
            },
            backgroundSize: {
                '0-px': '0 1px',
                'full-px': '100% 1px',
            },
            backgroundPosition: {
                'left-3/5': 'left 60%'
            }
        },
    },
    plugins: [],
}

