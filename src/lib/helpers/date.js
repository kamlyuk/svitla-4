const helperDate = {};

helperDate.urlToDateString = url => url.slice(1).replaceAll('/', '-');

// Check it matches the format "/0000/00/00" and it is a real date
helperDate.isUrlRealDate = url => url.match(/^\/\d{4}\/\d{2}\/\d{2}$/g) || !isNaN(Date.parse(helperDate.urlToDateString(url)))

helperDate.isOlder = (newDate, oldDate) => new Date(newDate).getTime() > new Date(oldDate).getTime();

helperDate.splitByFormats = (dateString, formats) => {

    const date = new Date(dateString);
    const response = [];

    formats.forEach(format => {

        switch (format) {
            case 'year':
                response.push(date.getFullYear());
                break;

            case 'monthNumberPadded':
                response.push(("0" + (date.getMonth() + 1)).slice(-2));
                break;

            case 'monthText':
                response.push(date.toLocaleString('en-US', {month: 'long'}));
                break;

            case 'dayNumberPadded':
                response.push(("0" + date.getDate()).slice(-2));
                break;

            case 'dayNumber':
                response.push(date.getDate());
                break;

            case 'dayWeekText':
                response.push(date.toLocaleString('en-US', {weekday: 'long'}));
                break;
        }
    });

    return response;
}

export default helperDate;