const helperTasks = {};

helperTasks.getAll = () => JSON.parse(localStorage.getItem('tasks'));

helperTasks.getByDate = dateString => JSON.parse(localStorage.getItem('tasks')).filter(task => task.date === dateString);

helperTasks.set = tasks => localStorage.setItem('tasks', JSON.stringify(tasks));

helperTasks.add = (text, dateString) => {

    const tasks = helperTasks.getAll();

    const highestId = tasks.reduce((highestId, task) => {

        if (typeof highestId === 'undefined') return task.id;

        return task.id > highestId ? task.id : highestId;
    });

    tasks.push({
        id: highestId + 1,
        date: dateString,
        text,
        isDone: false
    });

    helperTasks.set(tasks);
}

helperTasks.setStatus = (id, status) => {

    const tasks = helperTasks.getAll();

    const tasksUpdated = tasks.map(task => {

        if (task.id === id) {

            task.isDone = status;
        }

        return task;
    });

    helperTasks.set(tasksUpdated);
}

helperTasks.getAddText = () => {
    const addText = localStorage.getItem('taskAddText')

    return addText ? addText : '';
};

helperTasks.setAddText = text => {

    localStorage.setItem('taskAddText', text);
}


export default helperTasks;