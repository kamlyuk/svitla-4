export default [
    {
        id: 1,
        date: "2020-12-22",
        text: "Buy new sweatshirt",
        isDone: true
    },
    {
        id: 2,
        date: "2020-12-22",
        text: "Read an article",
        isDone: true
    },
    {
        id: 3,
        date: "2020-12-22",
        text: "Write blog post",
        isDone: false
    },
    {
        id: 4,
        date: "2020-12-22",
        text: "Watch \"Mr Robot\"",
        isDone: false
    },
    {
        id: 5,
        date: "2020-12-22",
        text: "Run",
        isDone: false
    }
]