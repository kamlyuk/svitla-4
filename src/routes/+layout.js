export const ssr = false;

import tasksInitial from '$lib/data/tasks-initial.js';

export function load() {

    return {tasksInitial};
}